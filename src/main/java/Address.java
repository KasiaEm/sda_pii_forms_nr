import java.util.Objects;

public class Address {
    public enum Country{
        PL, UK, US, FR, ES, GE
    }

    private Country country;
    private String state;
    private String city;
    private String postalCode;
    private String streetAddress;

    public Address(Country country, String state, String city, String postalCode, String streetAddress) {
        this.country = country;
        this.state = state;
        this.city = city;
        this.postalCode = postalCode;
        this.streetAddress = streetAddress;
    }

    public Country getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    @Override
    public String toString() {
        return "Address{" +
                "country=" + country +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", streetAddress='" + streetAddress + '\'' +
                '}';
    }

}
