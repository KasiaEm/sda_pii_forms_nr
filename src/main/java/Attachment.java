import java.util.Objects;

public class Attachment {
    public enum AttachmentType{
        PDF, IMAGE, XML, VIDEO
    }

    public Attachment(String path, String name, AttachmentType attachmentType) {
        this.path = path;
        this.name = name;
        this.attachmentType = attachmentType;
    }

    private String path;
    private String name;
    private AttachmentType attachmentType;

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public AttachmentType getAttachmentType() {
        return attachmentType;
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "path='" + path + '\'' +
                ", name='" + name + '\'' +
                ", attachmentType=" + attachmentType +
                '}';
    }
}
