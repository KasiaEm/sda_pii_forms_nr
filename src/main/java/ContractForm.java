import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ContractForm extends Form{
    public enum ContractType{
        CONTRACT_OF_EMPLOYMENT, B2B, CONTRACT_OF_WORK, CONTRACT_OF_MANDATE
    }

    public ContractForm(FormStyle formStyle, String contractId) {
        super(formStyle);
        this.contractId = contractId;
    }

    private String contractId;
    private ContractType contractType;
    private LocalDate date;
    private LocalDate dateDue;
    private List<Attachment> attachments = new LinkedList<>();
    private Subject employer;
    private Subject employee;
    private Payment payment;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public ContractType getContractType() {
        return contractType;
    }

    public void setContractType(ContractType contractType) {
        this.contractType = contractType;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getDateDue() {
        return dateDue;
    }

    public void setDateDue(LocalDate dateDue) {
        this.dateDue = dateDue;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public Subject getEmployer() {
        return employer;
    }

    public void setEmployer(Subject employer) {
        this.employer = employer;
    }

    public Subject getEmployee() {
        return employee;
    }

    public void setEmployee(Subject employee) {
        this.employee = employee;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    @Override
    public String toString() {
        return "ContractForm{" +
                "contractId='" + contractId + '\'' +
                ", contractType=" + contractType +
                ", date=" + date +
                ", dateDue=" + dateDue +
                ", attachments=" + attachments +
                ", employer=" + employer +
                ", employee=" + employee +
                ", payments=" + payment +
                '}';
    }
}
