public abstract class Form{
    public enum FormStyle{
        OFFICIAL, PLAIN, DARK, LIGHT
    }
    private FormStyle formStyle;

    public Form() {
    }

    public Form(FormStyle formStyle) {
        this.formStyle = formStyle;
    }

    public FormStyle getFormStyle() {
        return formStyle;
    }

    public void setFormStyle(FormStyle formStyle) {
        this.formStyle = formStyle;
    }

    @Override
    public String toString() {
        return "Form{" +
                "formStyle=" + formStyle +
                '}';
    }
}
