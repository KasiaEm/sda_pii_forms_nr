import java.util.Objects;

public class Subject {
    public enum SubjectType {
        PERSON, CORPORATION, COMPANY, SELF_EMPLOYMENT, FOUNDATION, SOCIETY
    }

    private String fullName;
    private String subjectName;
    private Address address;
    private String id;
    private String subjectId;
    private String email;
    private String phone;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "fullName='" + fullName + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", address=" + address +
                ", id='" + id + '\'' +
                ", subjectId='" + subjectId + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

}
